import { CustomerAppTwoPage } from './app.po';

describe('customer-app-two App', () => {
  let page: CustomerAppTwoPage;

  beforeEach(() => {
    page = new CustomerAppTwoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
